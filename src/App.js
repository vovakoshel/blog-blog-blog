import React, { Component } from "react";
import Navbar from "./components/layouts/Navbar";
import Homepage from "./components/pages/Homapage";
import SignIn from "./components/pages/SignIn";
import SignUp from "./components/pages/SignUp";
import Dashboard from "./components/Dashboard";
import AddArticle from "./components/AddArticle";
import EditArticle from "./components/EditArticle";
import PageNotFound from "./components/pages/PageNotFound";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Switch>
            <Route path="/" exact={true} component={Homepage} />
            <Route path="/SignUp" exact={true} component={SignUp} />
            <Route path="/SignIn" exact={true} component={SignIn} />
            <Route path="/Dashboard" exact={true} component={Dashboard} />
            <Route path="/article/add" exact={true} component={AddArticle} />
            <Route path="/article/edit/:id" component={EditArticle} />
            <Route component={PageNotFound} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
