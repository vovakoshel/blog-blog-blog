import React, { Component } from "react";
import axios from "../../axios-config";
export class SignUp extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmpassword: "",
    companyName: ""
  };

  onChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { firstName, lastName, email, companyName, password } = this.state;

    const formData = {
      name: companyName,
      user: {
        name: firstName,
        surname: lastName,
        password: password,
        mail: email
      }
    };

    axios
      .post("/Registration", formData)
      .then(res => this.props.history.push("/SignIn"))
      .catch(err => console.log(err));
  };

  render() {
    const {
      values: firstName,
      lastName,
      email,
      companyName,
      password,
      confirmpassword
    } = this.state;

    const isInvalid =
      firstName === "" ||
      lastName === "" ||
      companyName === "" ||
      email === "" ||
      password !== confirmpassword ||
      password === "";

    return (
      <div>
        <header id="sign-up-page">
          <div className="dark-overlay-sign-in">
            <div className="home-inner-sign-in">
              <div className="container">
                <div className="row">
                  <div className="col-lg-7 m-auto">
                    <div className="card bg-primary text-center card-form">
                      <div className="card-body">
                        <h3>Sign Up Today</h3>
                        <p>Please fill out this form to register</p>
                        <form onSubmit={this.onSubmit}>
                          <div className="form-group row">
                            <label
                              htmlFor="firstName"
                              className="col-sm-3 col-form-label"
                            >
                              First Name
                            </label>
                            <div className="col-sm-9">
                              <input
                                type="text"
                                className="form-control"
                                name="firstName"
                                value={firstName}
                                onChange={this.onChange}
                              />
                            </div>
                          </div>

                          <div className="form-group row">
                            <label
                              htmlFor="lastName"
                              className="col-sm-3 col-form-label"
                            >
                              Last Name
                            </label>
                            <div className="col-sm-9">
                              <input
                                type="text"
                                className="form-control"
                                name="lastName"
                                value={lastName}
                                onChange={this.onChange}
                              />
                            </div>
                          </div>
                          <div className="form-group row">
                            <label
                              htmlFor="companyName"
                              className="col-sm-3 col-form-label"
                            >
                              Company Name
                            </label>
                            <div className="col-sm-9">
                              <input
                                type="text"
                                className="form-control"
                                name="companyName"
                                value={companyName}
                                onChange={this.onChange}
                              />
                            </div>
                          </div>
                          <div className="form-group row">
                            <label
                              htmlFor="Email"
                              className="col-sm-3 col-form-label"
                            >
                              Email
                            </label>
                            <div className="col-sm-9">
                              <input
                                type="email"
                                className="form-control"
                                name="email"
                                value={email}
                                onChange={this.onChange}
                              />
                            </div>
                          </div>
                          <div className="form-group row">
                            <label
                              htmlFor="Password"
                              className="col-sm-3 col-form-label"
                            >
                              Password
                            </label>
                            <div className="col-sm-9">
                              <input
                                type="password"
                                className="form-control"
                                name="password"
                                value={password}
                                onChange={this.onChange}
                              />
                            </div>
                          </div>

                          <div className="form-group row">
                            <label
                              htmlFor="Email"
                              className="col-sm-3 col-form-label"
                            >
                              Confirm Passw
                            </label>
                            <div className="col-sm-9">
                              <input
                                type="password"
                                className="form-control"
                                name="confirmpassword"
                                value={confirmpassword}
                                onChange={this.onChange}
                              />
                            </div>
                          </div>

                          <input
                            type="submit"
                            className="btn btn-outline-light btn-block"
                            value="Sign Up"
                            disabled={isInvalid}
                          />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default SignUp;
