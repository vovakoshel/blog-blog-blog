import React, { Component } from "react";
import axios from "../../axios-config";
// const path = "http://89.223.92.96:8080/Blog";

class SignIn extends Component {
  state = {
    password: "",
    email: ""
  };

  onChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { email, password } = this.state;

    const userData = {
      mail: email,
      password: password
    };

    axios
      .post(`/Authentication`, userData)
      .then(res => this.props.history.push("/dashboard"));

    // fetch(`/Authentication`, {
    //   method: "POST",
    //   credentials: "include",
    //   body: JSON.stringify({
    //     mail: email,
    //     password: password
    //   }),
    //   headers: {
    //     "Content-type": "application/json; charset=UTF-8"
    //   }
    // }).then(res => {
    //   this.props.history.push("/dashboard");
    // });
  };

  render() {
    const { email, password } = this.state;
    const isInvalid = email === "" || password === "";
    return (
      <div>
        <header id="sign-in-page">
          <div className="dark-overlay-sign-in">
            <div className="home-inner-sign-in">
              <div className="container">
                <div className="row">
                  <div className="col-lg-6 m-auto">
                    <div className="card bg-primary text-center card-form">
                      <div className="card-body">
                        <h3>Sign In </h3>
                        <p>Please fill out this form to register</p>
                        <form onSubmit={this.onSubmit}>
                          <div className="form-group">
                            <input
                              type="email"
                              name="email"
                              className="form-control"
                              placeholder="Email"
                              value={email}
                              onChange={this.onChange}
                            />
                          </div>
                          <div className="form-group">
                            <input
                              type="password"
                              name="password"
                              className="form-control"
                              placeholder="Password"
                              value={password}
                              onChange={this.onChange}
                            />
                          </div>

                          <input
                            type="submit"
                            className="btn btn-outline-light btn-block"
                            value="Sign In"
                            disabled={isInvalid}
                          />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
      </div>
    );
  }
}

export default SignIn;
