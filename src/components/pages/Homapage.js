import React, { Component } from "react";
import { Link } from "react-router-dom";
import bgSection2 from "../../img/bg6.jpeg";
import axios from "../../axios-config";

// const path = "http://89.223.92.96:8080/Blog";

export class Homapage extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    companyName: ""
  };

  onChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  //PROCESS FORM
  onSubmit = e => {
    e.preventDefault();
    const { firstName, lastName, email, companyName, password } = this.state;

    const formData = {
      name: companyName,
      user: {
        name: firstName,
        surname: lastName,
        password: password,
        mail: email
      }
    };
    axios
      .post("/Registration", formData)
      .then(res => this.props.history.push("/SignIn"))
      .catch(err => console.log(err));

    // fetch(`${path}/Registration`, {
    //   method: "POST",
    //   body: JSON.stringify({
    //     name: companyName,
    //     user: {
    //       name: firstName,
    //       surname: lastName,
    //       password: password,
    //       mail: email
    //     }
    //   }),
    //   headers: {
    //     "Content-type": "application/json; charset=UTF-8"
    //   }
    // }).then(res => {
    //   this.props.history.push("/SignIn");
    // });

    this.setState({
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      companyName: ""
    });
  };

  render() {
    const {
      values: firstName,
      lastName,
      email,
      companyName,
      password
    } = this.state;

    const isInvalid =
      firstName === "" ||
      lastName === "" ||
      companyName === "" ||
      email === "" ||
      password === "";

    return (
      <div>
        <header id="home-section">
          <div className="dark-overlay">
            <div className="home-inner">
              <div className="container">
                <div className="row">
                  <div className="col-lg-8 d-none d-lg-block">
                    <h1 className="display-4">The Easietst way to blog</h1>
                    <div className="d-flex flex-row">
                      <div className="p-4 align-self-start">
                        <i className="fa fa-check" />
                      </div>
                      <div className="p-4 align-self-end">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Excepturi dignissimos nemo laboriosam aperiam mollitia
                        suscipit?
                      </div>
                    </div>
                    <div className="d-flex flex-row">
                      <div className="p-4 align-self-start">
                        <i className="fa fa-check" />
                      </div>
                      <div className="p-4 align-self-end">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Excepturi dignissimos nemo laboriosam aperiam mollitia
                        suscipit?
                      </div>
                    </div>
                    <div className="d-flex flex-row">
                      <div className="p-4 align-self-start">
                        <i className="fa fa-check" />
                      </div>
                      <div className="p-4 align-self-end">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Excepturi dignissimos nemo laboriosam aperiam mollitia
                        suscipit?
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4">
                    <div className="card bg-primary text-center card-form">
                      <div className="card-body">
                        <h3>Sign Up Today</h3>
                        <p>Please fill out this form to register</p>
                        <form onSubmit={this.onSubmit}>
                          <div className="form-group">
                            <input
                              type="text"
                              className="form-control"
                              name="firstName"
                              value={firstName}
                              onChange={this.onChange}
                              placeholder="First Name"
                            />
                          </div>
                          <div className="form-group">
                            <input
                              type="text"
                              className="form-control"
                              name="lastName"
                              value={lastName}
                              onChange={this.onChange}
                              placeholder="Last Name"
                            />
                          </div>
                          <div className="form-group">
                            <input
                              type="text"
                              className="form-control"
                              name="companyName"
                              value={companyName}
                              onChange={this.onChange}
                              placeholder="Name of Your Company"
                            />
                          </div>
                          <div className="form-group">
                            <input
                              type="email"
                              className="form-control"
                              name="email"
                              value={email}
                              onChange={this.onChange}
                              placeholder="Email"
                            />
                          </div>
                          <div className="form-group">
                            <input
                              type="password"
                              className="form-control"
                              name="password"
                              value={password}
                              onChange={this.onChange}
                              placeholder="password"
                            />
                          </div>
                          <input
                            type="submit"
                            className="btn btn-outline-light btn-block"
                            value="Sign Up"
                            // data-toggle="modal"
                            // data-target="#contactModal"
                            disabled={isInvalid}
                          />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>

        <section id="explore-head-section">
          <div className="container">
            <div className="row">
              <div className="col text-center">
                <div className="p-5">
                  <h1 className="display-6">Explore for yourself</h1>
                  <p className="lead">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Qui, maiores est in esse minima laborum eveniet quae ipsa
                    quam inventore, repellendus eaque temporibus blanditiis
                    minus eos, vel natus soluta eum! lorem
                  </p>
                  <Link to="/" className="btn btn-outline-secondary">
                    Find Out More
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section id="explore-section" className="bg-light text-muted py-5">
          <div className="container">
            <div className="row">
              <div className="col-md-6">
                <img
                  src={bgSection2}
                  alt=""
                  className="img-fluid mb-3 rounded-circle"
                />
              </div>
              <div className="col-md-6">
                <h3>Explore & Connect</h3>
                <p>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit.
                  Dolores maxime alias fuga laudantium minus repellat aut illo
                  cumque, enim a laborum perspiciatis! Beatae nam impedit ab
                  nobis dolore nemo distinctio!
                </p>
                <div className="d-flex flex-row">
                  <div className="p-4 align-self-start">
                    <i className="fa fa-check" />
                  </div>
                  <div className="p-4 align-self-end">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Excepturi dignissimos nemo laboriosam aperiam mollitia
                    suscipit?
                  </div>
                </div>
                <div className="d-flex flex-row">
                  <div className="p-4 align-self-start">
                    <i className="fa fa-check" />
                  </div>
                  <div className="p-4 align-self-end">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Excepturi dignissimos nemo laboriosam aperiam mollitia
                    suscipit?
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* <CONTACT MODAL */}
        <div className="modal fade text-dark" id="contactModal">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title text-justify" id="contactModalTitle">
                  Thanks For Registration
                </h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Homapage;
