import React from "react";

const PageNotFound = () => {
  return (
    <div style={{ textAlign: "center", marginTop: "150px" }}>
      <h1>PAGE NOT FOUND</h1>
    </div>
  );
};

export default PageNotFound;
