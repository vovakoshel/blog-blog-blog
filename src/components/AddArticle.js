import Editor from "./Editor";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import React, { Component } from "react";
// const path = "http://89.223.92.96:8080/Blog";
import axios from "../axios-config";

class AddArticle extends Component {
  state = {
    title: "",
    body: ""
  };

  handleChangeTitle = value => {
    this.setState({ title: value });
  };
  handleChangeBody = value => {
    this.setState({ body: value });
  };

  addArticle = async (title, body) => {
    const article = {
      title: title,
      body: body,
      status: 2
    };
    try {
      await axios.post(`/AddArticle`, article);
      this.props.history.push("/dashboard");
    } catch (err) {
      console.log(err);
    }
  };

  addArticleToDraft = async (title, body) => {
    const article = {
      title: title,
      body: body,
      status: 1
    };
    try {
      await axios.post(`/AddArticle`, article);
      this.props.history.push("/dashboard");
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { title, body } = this.state;
    return (
      <div className="container" style={{ marginTop: "100px" }}>
        <h2 className="fontheader mb-5 mt-3">Create your article</h2>
        <br />
        <Editor
          valueTitle={this.state.title}
          valueBody={this.state.body}
          handleChangeTitle={this.handleChangeTitle}
          handleChangeBody={this.handleChangeBody}
        />

        <span
          onClick={this.addArticle.bind(this, title, body)}
          style={{ float: "right", cursor: "pointer", fontSize: 25 }}
        >
          <i className="far fa-clipboard" /> Publish
        </span>
        <span
          onClick={this.addArticleToDraft.bind(this, title, body)}
          style={{
            float: "right",
            cursor: "pointer",
            fontSize: 25,
            marginRight: 40
          }}
        >
          <i className="far fa-save" /> Save
        </span>
      </div>
    );
  }
}

export default AddArticle;
