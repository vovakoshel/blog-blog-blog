import React, { Component } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";

class Editor extends Component {
  handleChangeTitle = value => {
    this.props.handleChangeTitle(value);
  };
  handleChangeBody = value => {
    this.props.handleChangeBody(value);
  };

  render() {
    // console.log(this.props.valueTitle);
    return (
      <div>
        <ReactQuill
          value={this.props.valueTitle}
          onChange={this.handleChangeTitle}
          theme="bubble"
          placeholder="Title..."
        />
        <hr />
        <ReactQuill
          value={this.props.valueBody}
          onChange={this.handleChangeBody}
          theme="bubble"
          placeholder="Body..."
          modules={Editor.modules}
          formats={Editor.formats}
        />
        <hr />
      </div>
    );
  }
}

Editor.modules = {
  toolbar: [
    ["bold", "italic", "underline", "strike"], // toggled buttons
    ["blockquote", "code-block"],

    [{ header: 1 }, { header: 2 }], // custom button values
    [{ list: "ordered" }, { list: "bullet" }],

    // [{ direction: "rtl" }], // text direction

    [{ size: ["small", false, "large", "huge"] }], // custom dropdown
    [{ header: [1, 2, 3, 4, 5, 6, false] }],

    // [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    [{ font: [] }],

    ["clean"] // remove formatting button
  ]
};
/*
 * Quill editor formats
 * See https://quilljs.com/docs/formats/
 */
Editor.formats = [
  "header",
  "font",
  "size",
  "bold",
  "italic",
  "underline",
  "strike",
  "blockquote",
  "link",
  "list",
  "bullet",
  "indent",
  "code-block"
];

export default Editor;
