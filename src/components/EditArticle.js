import Editor from "./Editor";
import "react-quill/dist/quill.snow.css";
import "react-quill/dist/quill.bubble.css";
import React, { Component } from "react";
import axios from "../axios-config";
// const path = "http://89.223.92.96:8080/Blog";

class EditArticle extends Component {
  state = {
    title: "",
    body: "",
    status: null
  };

  async componentDidMount() {
    const { id } = this.props.match.params;
    try {
      const article = await axios.get(`/ViewArticleById/${id}`);
      this.setState({
        body: article.data.body,
        title: article.data.title,
        status: article.data.status
      });
    } catch (err) {
      console.log(err);
    }
    // const article = await fetch(`/ViewArticleById/${id}`, {
    //   credentials: "include"
    // }).then(response => response.json());
    // this.setState({
    //   body: article.body,
    //   title: article.title,
    //   status: article.status
    // });
  }

  handleChangeBody = value => {
    this.setState({ body: value });
  };
  handleChangeTitle = value => {
    this.setState({ title: value });
  };

  editArticle = async (body, title, id, status) => {
    try {
      const changeArticle = {
        title: title,
        body: body,
        status: status
      };
      await axios.put(`/UpdateArticle/${id}`, changeArticle);
      this.props.history.push("/Dashboard");
    } catch (err) {
      console.log(err);
    }

    // await fetch(`${path}/UpdateArticle/${id}`, {
    //   method: "PUT",
    //   credentials: "include",
    //   body: JSON.stringify({
    //     title: title,
    //     body: body,
    //     status: status
    //   }),
    //   headers: {
    //     "Content-type": "application/json; charset=UTF-8"
    //   }
    // });
  };

  changeArticleStatus = async (body, title, id) => {
    try {
      const changeArticle = {
        title: title,
        body: body,
        status: 2
      };
      await axios.put(`/UpdateArticle/${id}`, changeArticle);
      this.props.history.push("/Dashboard");
    } catch (err) {
      console.log(err);
    }
    // await fetch(`${path}/UpdateArticle/${id}`, {
    //   method: "PUT",
    //   credentials: "include",
    //   body: JSON.stringify({
    //     title: title,
    //     body: body,
    //     status: 2
    //   }),
    //   headers: {
    //     "Content-type": "application/json; charset=UTF-8"
    //   }
    // });
    // this.props.history.push("/Dashboard");
  };

  render() {
    const { title, body, status } = this.state;
    const { id } = this.props.match.params;
    return (
      <div className="container" style={{ marginTop: "100px" }}>
        <h2 className="fontheader mb-5 mt-3">Edit your article</h2>
        <Editor
          valueTitle={title}
          valueBody={body}
          handleChangeTitle={this.handleChangeTitle}
          handleChangeBody={this.handleChangeBody}
        />

        <span
          onClick={this.editArticle.bind(this, body, title, id, status)}
          style={{ float: "right", cursor: "pointer", fontSize: 25 }}
        >
          <i className="far fa-edit" /> Edit
        </span>

        {status !== 2 ? (
          <span
            onClick={this.changeArticleStatus.bind(this, body, title, id)}
            style={{
              float: "right",
              cursor: "pointer",
              fontSize: 25,
              marginRight: "20px"
            }}
          >
            <i className="far fa-clipboard" /> Publish
          </span>
        ) : null}
      </div>
    );
  }
}

export default EditArticle;
