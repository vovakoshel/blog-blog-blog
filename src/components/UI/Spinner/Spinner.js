import React from "react";
import { css } from "@emotion/core";
import { PacmanLoader } from "react-spinners";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

const spinner = props => (
  <PacmanLoader
    css={override}
    sizeUnit={"px"}
    size={props.size}
    color={"#008ed6"}
  />
);

export default spinner;
