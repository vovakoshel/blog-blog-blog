import React, { Component } from "react";
import Article from "./Article";
import axios from "../axios-config";
import Spinner from "../components/UI/Spinner/Spinner";
import { Link } from "react-router-dom";

export class Dashboard extends Component {
  state = {
    loading: true,
    articles: [],
    wordToMatch: ""
  };

  onChange = e => {
    const { value, name } = e.target;
    this.setState({
      [name]: value
    });
  };

  fetchAllArticles = async () => {
    try {
      const response = await axios.get("/ViewAllArticles");
      this.setState({
        articles: response.data.reverse(),
        loading: false
      });
    } catch (error) {
      console.error(error);
    }
  };

  componentDidMount() {
    this.fetchAllArticles();
  }

  deleteArticleHandler = id => {
    this.setState({
      articles: this.state.articles.filter(article => article.id !== id)
    });

    axios.delete(`/DeleteArticle/${id}`);
  };

  changeStatusToHidden = (id, title, body) => {
    const post = {
      title: title,
      body: body,
      status: 0
    };
    axios.put(`/UpdateArticle/${id}`, post);

    this.fetchAllArticles();
  };

  changeStatusToShown = (id, title, body) => {
    const post = {
      title: title,
      body: body,
      status: 2
    };
    axios.put(`/UpdateArticle/${id}`, post);

    this.fetchAllArticles();
  };

  onClickShareLinkHandler = () => {
    fetch(`http://localhost:8080/Blog/LinkForFriends`, {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    }).then(res => console.log(res.json()));

    // axios.post("/LinkForFriends").then(res => {
    //   console.log(res.data);
    // });
  };

  render() {
    const { articles, loading } = this.state;

    let filteredArticles = articles.filter(article => {
      // return article.title.indexOf(this.state.wordToMatch) !== -1;
      const regex = new RegExp(this.state.wordToMatch, "gi");
      return article.title.match(regex);
    });

    let posts = filteredArticles.map(article => (
      <Article
        key={article.id}
        article={article}
        deleteArticle={this.deleteArticleHandler}
        changeStatusToHidden={this.changeStatusToHidden}
        changeStatusToShown={this.changeStatusToShown}
      />
    ));
    if (loading) {
      posts = <Spinner size="50" />;
    }
    return (
      <div className="container">
        <div className="articles-container ">
          <div class="input-group mb-3 ">
            <input
              type="text"
              class="form-control "
              placeholder="search article ..."
              value={this.state.wordToMatch}
              onChange={this.onChange}
              name="wordToMatch"
            />
            <div class="input-group-append">
              <select class="custom-select">
                <option selected>Choose Filter</option>
                <option value="hidden">Hidden</option>
                <option value="published">Published</option>
                <option value="unPublished">UnPublished</option>
              </select>
            </div>
            <div class="input-group-append">
              <Link to="/article/add">
                <button className="btn btn-info">
                  <i className="fas fa-plus" /> Create Article
                </button>
              </Link>
            </div>
            <div class="input-group-append">
              <button
                className="btn btn-success"
                onClick={this.onClickShareLinkHandler}
              >
                <i className="fas fa-share " /> Share With Friends
              </button>
            </div>
          </div>

          {posts}
        </div>
      </div>
    );
  }
}

export default Dashboard;
